package edu.ib.springsecuritydemo.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserDtoRepo extends CrudRepository<UserDto, String> {
}
