package edu.ib.springsecuritydemo.repository;

import edu.ib.springsecuritydemo.configuration.PasswordEncoderConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.factory.PasswordEncoderFactories;
import org.springframework.security.crypto.password.PasswordEncoder;

public class UserDtoBuilder {

    PasswordEncoder passwordEncoder = new PasswordEncoderConfig().passwordEncoder();

    @Bean
    public UserDto userToUserDto(User user) {
        UserDto userDto = new UserDto();
        userDto.setName(user.getName());
        userDto.setPasswordHash(passwordEncoder.encode(user.getPassword()));
        userDto.setRole(user.getRole());
        return userDto;
    }
}
