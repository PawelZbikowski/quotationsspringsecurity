package edu.ib.springsecuritydemo.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

import javax.sql.DataSource;

@Configuration
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private DataSource dataSource;
    @Autowired
    private PasswordEncoder passwordEncoder;

//    @Bean
//    public UserDetailsService userDetailsService() {
//        UserDetails moderator = User.withDefaultPasswordEncoder()
//                .username("moderator")
//                .password("moderator1")
//                .roles("MODERATOR")
//                .build(); // tworzenie moderatora
//        UserDetails admin = User.withDefaultPasswordEncoder()
//                .username("admin")
//                .password("admin1")
//                .roles("ADMIN")
//                .build(); // tworzenie administratora
//
//        return new InMemoryUserDetailsManager(moderator, admin); // zapisanie go w pamięci
//    }


    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.jdbcAuthentication()
                .usersByUsernameQuery("SELECT u.name, u.password_hash, 1 FROM user_dto u WHERE u.name=?")
                .authoritiesByUsernameQuery("SELECT u.name, u.role, 1 FROM user_dto u WHERE u.name=?")
                .dataSource(dataSource)
                .passwordEncoder(passwordEncoder);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.httpBasic().and().authorizeRequests()
                .antMatchers("/")
                .permitAll()
                .antMatchers("/console/**")
                .permitAll()
                .antMatchers(HttpMethod.GET,"/api")
                .permitAll() // do endpointa "/api" i żądania GET dostęp mają WSZYSCY UŻYTKOWNICY
                .antMatchers(HttpMethod.POST,"/api")
                .hasAnyRole("MODERATOR", "ADMIN") // do endpointa "/api" i żądania POST dostęp mają MODERATORZY i ADMINI
                .antMatchers(HttpMethod.DELETE,"/api")
                .hasRole("ADMIN") // do endpointa "/api" i żądania DELETE dostęp mają ADMINISTRATORZY
                .and()
                .formLogin()
                .permitAll() // dodanie formularza z logowaniem (dostarczony wraz ze Spring Security
                .and()
                .logout()
                .permitAll() // dodanie opcji wylogowania
                .and()
                .csrf().disable(); /* wylaczenie opcji blokowania CSRF przez Spring Security -
                dzięki temu mozna wysylac zadania typu POST i DELETE z POSTMANA*/

                http.headers().frameOptions().disable();

    }
}
