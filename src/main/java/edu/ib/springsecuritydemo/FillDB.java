package edu.ib.springsecuritydemo;

import edu.ib.springsecuritydemo.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class FillDB {

    private UserDtoRepo userDtoRepo;

    @Autowired
    public FillDB(UserDtoRepo userDtoRepo) {
        this.userDtoRepo = userDtoRepo;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void fillDataBase() {
        User user = new User("andrzeju", "qwer1234", "ROLE_CUSTOMER");
        User admin = new User("adminior", "qwerty", "ROLE_ADMIN");
        UserDto userDto;
        userDto = new UserDtoBuilder().userToUserDto(user);
        UserDto adminDto;
        adminDto = new UserDtoBuilder().userToUserDto(admin);
        userDtoRepo.save(userDto);
        userDtoRepo.save(adminDto);
    }
}
