package edu.ib.springsecuritydemo;

import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class QuotationController {

    private List<Quotation> quotations; // tworzymy liste cytatow

    public QuotationController() {
        this.quotations = new ArrayList<>();
        quotations.add(new Quotation("Można, jeszcze jak!", "JPII"));
        quotations.add(new Quotation("No i Pan Paweł", "Kolega Pana Pawła"));
    }

    @GetMapping("/api")
    public List<Quotation> getQuotations() {
        return quotations;
    }

    @PostMapping("/api")
    public boolean addQuotation(@RequestBody Quotation quotation) {
        return quotations.add(quotation);
    }

    @DeleteMapping("/api")
    public void deleteQuotation(@RequestParam int index) {
        quotations.remove(index);
    }

}
