package edu.ib.springsecuritydemo.service;

import edu.ib.springsecuritydemo.repository.UserDto;
import edu.ib.springsecuritydemo.repository.UserDtoRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserDtoService {

    private UserDtoRepo userDtoRepo;

    @Autowired
    public UserDtoService(UserDtoRepo userDtoRepo) {
        this.userDtoRepo = userDtoRepo;
    }

    public Optional<UserDto> findByName(String name) {
        return userDtoRepo.findById(name);
    }

    public UserDto save(UserDto userDto) {
        return userDtoRepo.save(userDto);
    }
}
